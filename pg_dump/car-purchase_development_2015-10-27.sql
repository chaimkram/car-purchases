--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: cars; Type: TABLE; Schema: public; Owner: car-purchase; Tablespace: 
--

CREATE TABLE cars (
    id integer NOT NULL,
    name character varying,
    year character varying,
    price integer,
    kilometrage integer,
    current_owner character varying,
    phone integer,
    address character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.cars OWNER TO "car-purchase";

--
-- Name: cars_id_seq; Type: SEQUENCE; Schema: public; Owner: car-purchase
--

CREATE SEQUENCE cars_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cars_id_seq OWNER TO "car-purchase";

--
-- Name: cars_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: car-purchase
--

ALTER SEQUENCE cars_id_seq OWNED BY cars.id;


--
-- Name: criteria; Type: TABLE; Schema: public; Owner: car-purchase; Tablespace: 
--

CREATE TABLE criteria (
    id integer NOT NULL,
    name character varying,
    comments text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.criteria OWNER TO "car-purchase";

--
-- Name: criteria_id_seq; Type: SEQUENCE; Schema: public; Owner: car-purchase
--

CREATE SEQUENCE criteria_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.criteria_id_seq OWNER TO "car-purchase";

--
-- Name: criteria_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: car-purchase
--

ALTER SEQUENCE criteria_id_seq OWNED BY criteria.id;


--
-- Name: criteria_rankings; Type: TABLE; Schema: public; Owner: car-purchase; Tablespace: 
--

CREATE TABLE criteria_rankings (
    id integer NOT NULL,
    criterium_id integer,
    user_id integer,
    ranking integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    notes text
);


ALTER TABLE public.criteria_rankings OWNER TO "car-purchase";

--
-- Name: criteria_rankings_id_seq; Type: SEQUENCE; Schema: public; Owner: car-purchase
--

CREATE SEQUENCE criteria_rankings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.criteria_rankings_id_seq OWNER TO "car-purchase";

--
-- Name: criteria_rankings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: car-purchase
--

ALTER SEQUENCE criteria_rankings_id_seq OWNED BY criteria_rankings.id;


--
-- Name: ratings; Type: TABLE; Schema: public; Owner: car-purchase; Tablespace: 
--

CREATE TABLE ratings (
    id integer NOT NULL,
    car_id integer,
    rating integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    user_id integer,
    notes text,
    criterium_id integer
);


ALTER TABLE public.ratings OWNER TO "car-purchase";

--
-- Name: ratings_id_seq; Type: SEQUENCE; Schema: public; Owner: car-purchase
--

CREATE SEQUENCE ratings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ratings_id_seq OWNER TO "car-purchase";

--
-- Name: ratings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: car-purchase
--

ALTER SEQUENCE ratings_id_seq OWNED BY ratings.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: car-purchase; Tablespace: 
--

CREATE TABLE schema_migrations (
    version character varying NOT NULL
);


ALTER TABLE public.schema_migrations OWNER TO "car-purchase";

--
-- Name: users; Type: TABLE; Schema: public; Owner: car-purchase; Tablespace: 
--

CREATE TABLE users (
    id integer NOT NULL,
    name character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.users OWNER TO "car-purchase";

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: car-purchase
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO "car-purchase";

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: car-purchase
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: car-purchase
--

ALTER TABLE ONLY cars ALTER COLUMN id SET DEFAULT nextval('cars_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: car-purchase
--

ALTER TABLE ONLY criteria ALTER COLUMN id SET DEFAULT nextval('criteria_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: car-purchase
--

ALTER TABLE ONLY criteria_rankings ALTER COLUMN id SET DEFAULT nextval('criteria_rankings_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: car-purchase
--

ALTER TABLE ONLY ratings ALTER COLUMN id SET DEFAULT nextval('ratings_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: car-purchase
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Data for Name: cars; Type: TABLE DATA; Schema: public; Owner: car-purchase
--

COPY cars (id, name, year, price, kilometrage, current_owner, phone, address, created_at, updated_at) FROM stdin;
1	Ford Focus Wagon	2011	65000	80000	Joe Blow	\N	12345 Ben Gurion Blvd., Kfar Saba	2015-10-15 20:24:24.235539	2015-10-15 20:24:24.235539
\.


--
-- Name: cars_id_seq; Type: SEQUENCE SET; Schema: public; Owner: car-purchase
--

SELECT pg_catalog.setval('cars_id_seq', 1, true);


--
-- Data for Name: criteria; Type: TABLE DATA; Schema: public; Owner: car-purchase
--

COPY criteria (id, name, comments, created_at, updated_at) FROM stdin;
3	Front seat comfort	Lumbar support, seat cushion for long trips	2015-10-01 15:54:53.881095	2015-10-01 15:54:53.881095
4	Rear seat comfort (2 people)	Leg room, mainly	2015-10-01 15:55:35.685637	2015-10-01 15:55:35.685637
5	Rear seat comfort (3 people)	Leg room as well as width (hip) for seating 3 adults; comfort of middle seat	2015-10-01 15:56:13.783417	2015-10-01 15:56:13.783417
6	Reliability		2015-10-01 15:56:36.222747	2015-10-01 15:56:36.222747
7	Ride comfort		2015-10-01 15:56:50.024989	2015-10-01 15:56:50.024989
8	Fuel economy		2015-10-01 15:57:21.212353	2015-10-01 15:57:21.212353
9	Color / looks		2015-10-01 20:04:35.152996	2015-10-01 20:04:35.152996
10	Heating / cooling system		2015-10-01 20:04:50.230619	2015-10-01 20:04:50.230619
11	iPhone integration	Includes audio input jack, Bluetooth, USB ports front/rear for charging	2015-10-01 20:05:18.872075	2015-10-01 20:05:18.872075
13	Cost		2015-10-01 20:05:43.113278	2015-10-01 20:05:43.113278
14	Safety	Side airbags?	2015-10-01 20:06:04.404924	2015-10-01 20:06:04.404924
15	Stick shift		2015-10-01 20:06:15.85176	2015-10-01 20:06:15.85176
1	Visibility	Include window sizes, rear visibility, rear view mirrors, blind spots	2015-10-01 15:54:09.026385	2015-10-07 19:26:23.41327
2	Handling / Drivability	includes acceleration	2015-10-01 15:54:35.122272	2015-10-12 17:43:41.285352
12	Trunk space		2015-10-01 20:05:28.653481	2015-10-15 16:13:59.176615
\.


--
-- Name: criteria_id_seq; Type: SEQUENCE SET; Schema: public; Owner: car-purchase
--

SELECT pg_catalog.setval('criteria_id_seq', 16, true);


--
-- Data for Name: criteria_rankings; Type: TABLE DATA; Schema: public; Owner: car-purchase
--

COPY criteria_rankings (id, criterium_id, user_id, ranking, created_at, updated_at, notes) FROM stdin;
3	2	1	4	2015-10-01 16:24:08.210501	2015-10-01 16:24:08.210501	\N
5	3	1	4	2015-10-01 20:08:31.659461	2015-10-01 20:08:31.659461	\N
6	3	2	5	2015-10-01 20:08:42.389688	2015-10-01 20:08:42.389688	\N
7	4	1	4	2015-10-01 20:08:54.96234	2015-10-01 20:08:54.96234	\N
8	4	2	4	2015-10-01 20:09:10.268494	2015-10-01 20:09:10.268494	\N
9	5	1	3	2015-10-01 20:09:33.07438	2015-10-01 20:09:33.07438	\N
10	5	2	5	2015-10-01 20:09:43.751106	2015-10-01 20:09:43.751106	\N
11	6	1	4	2015-10-01 20:09:57.198241	2015-10-01 20:09:57.198241	\N
12	6	2	5	2015-10-01 20:10:21.150069	2015-10-01 20:10:21.150069	\N
13	7	1	4	2015-10-01 20:11:04.601109	2015-10-01 20:11:04.601109	\N
14	7	2	2	2015-10-01 20:11:15.311479	2015-10-01 20:11:15.311479	\N
15	8	1	3	2015-10-01 20:11:27.948538	2015-10-01 20:11:27.948538	\N
16	8	2	5	2015-10-01 20:11:46.451207	2015-10-01 20:11:46.451207	\N
17	9	1	2	2015-10-01 20:11:58.872352	2015-10-01 20:11:58.872352	\N
18	9	2	1	2015-10-01 20:12:08.790536	2015-10-01 20:12:08.790536	\N
19	10	1	3	2015-10-01 20:12:35.081384	2015-10-01 20:12:35.081384	\N
20	10	2	4	2015-10-01 20:12:48.816071	2015-10-01 20:12:48.816071	\N
21	11	1	3	2015-10-01 20:13:01.602335	2015-10-01 20:13:01.602335	\N
22	11	2	3	2015-10-01 20:13:14.292446	2015-10-01 20:13:14.292446	\N
2	1	2	5	2015-10-01 16:20:27.12396	2015-10-12 05:30:31.074469	rear visibility in particular
1	1	1	4	2015-10-01 15:58:35.915195	2015-10-12 05:34:00.556815	all around
4	2	2	5	2015-10-01 16:25:04.089871	2015-10-12 05:34:26.241111	really, a five?
35	13	2	3	2015-10-14 19:56:57.227056	2015-10-14 19:56:57.227056	Chaim added this  :)
24	14	1	4	2015-10-07 19:25:03.596163	2015-10-07 19:25:03.596163	\N
36	15	2	3	2015-10-14 20:33:45.808931	2015-10-14 20:33:45.808931	Also added by Chaim...
37	15	1	5	2015-10-14 20:48:06.407866	2015-10-14 20:48:06.407866	You know you want it
34	14	2	5	2015-10-14 19:54:22.204519	2015-10-14 20:48:35.79077	
39	13	1	3	2015-10-14 20:50:53.029357	2015-10-14 20:51:00.826579	Eh
38	12	1	\N	2015-10-14 20:50:43.319527	2015-10-15 16:41:47.319191	hello there
48	16	1	\N	2015-10-20 14:53:04.498626	2015-10-20 14:53:04.498626	\N
49	16	2	\N	2015-10-20 14:56:24.742322	2015-10-20 14:56:24.742322	\N
\.


--
-- Name: criteria_rankings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: car-purchase
--

SELECT pg_catalog.setval('criteria_rankings_id_seq', 49, true);


--
-- Data for Name: ratings; Type: TABLE DATA; Schema: public; Owner: car-purchase
--

COPY ratings (id, car_id, rating, created_at, updated_at, user_id, notes, criterium_id) FROM stdin;
1	1	2	2015-10-18 15:26:47.81674	2015-10-18 15:26:47.81674	\N	\N	\N
2	1	4	2015-10-18 15:43:29.132447	2015-10-18 15:43:29.132447	\N	\N	\N
3	1	3	2015-10-20 05:49:19.540697	2015-10-20 05:49:19.540697	\N	\N	\N
\.


--
-- Name: ratings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: car-purchase
--

SELECT pg_catalog.setval('ratings_id_seq', 3, true);


--
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: public; Owner: car-purchase
--

COPY schema_migrations (version) FROM stdin;
20151001134942
20151001134954
20151001135303
20151001135405
20151001135453
20151001154934
20151012044923
20151012045906
20151015162523
20151021070054
20151025195100
20151026175806
20151026180321
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: car-purchase
--

COPY users (id, name, created_at, updated_at) FROM stdin;
1	Chaim	2015-10-01 15:58:03.712843	2015-10-01 15:58:03.712843
2	Ariella	2015-10-01 15:58:12.515486	2015-10-01 15:58:12.515486
\.


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: car-purchase
--

SELECT pg_catalog.setval('users_id_seq', 2, true);


--
-- Name: cars_pkey; Type: CONSTRAINT; Schema: public; Owner: car-purchase; Tablespace: 
--

ALTER TABLE ONLY cars
    ADD CONSTRAINT cars_pkey PRIMARY KEY (id);


--
-- Name: criteria_pkey; Type: CONSTRAINT; Schema: public; Owner: car-purchase; Tablespace: 
--

ALTER TABLE ONLY criteria
    ADD CONSTRAINT criteria_pkey PRIMARY KEY (id);


--
-- Name: criteria_rankings_pkey; Type: CONSTRAINT; Schema: public; Owner: car-purchase; Tablespace: 
--

ALTER TABLE ONLY criteria_rankings
    ADD CONSTRAINT criteria_rankings_pkey PRIMARY KEY (id);


--
-- Name: ratings_pkey; Type: CONSTRAINT; Schema: public; Owner: car-purchase; Tablespace: 
--

ALTER TABLE ONLY ratings
    ADD CONSTRAINT ratings_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: car-purchase; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: index_criteria_rankings_on_criterium_id_and_user_id; Type: INDEX; Schema: public; Owner: car-purchase; Tablespace: 
--

CREATE UNIQUE INDEX index_criteria_rankings_on_criterium_id_and_user_id ON criteria_rankings USING btree (criterium_id, user_id);


--
-- Name: unique_schema_migrations; Type: INDEX; Schema: public; Owner: car-purchase; Tablespace: 
--

CREATE UNIQUE INDEX unique_schema_migrations ON schema_migrations USING btree (version);


--
-- Name: fk_rails_0376098a68; Type: FK CONSTRAINT; Schema: public; Owner: car-purchase
--

ALTER TABLE ONLY ratings
    ADD CONSTRAINT fk_rails_0376098a68 FOREIGN KEY (criterium_id) REFERENCES criteria(id);


--
-- Name: fk_rails_a7dfeb9f5f; Type: FK CONSTRAINT; Schema: public; Owner: car-purchase
--

ALTER TABLE ONLY ratings
    ADD CONSTRAINT fk_rails_a7dfeb9f5f FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: fk_rails_bdff5284e4; Type: FK CONSTRAINT; Schema: public; Owner: car-purchase
--

ALTER TABLE ONLY ratings
    ADD CONSTRAINT fk_rails_bdff5284e4 FOREIGN KEY (car_id) REFERENCES cars(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: chaimk
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM chaimk;
GRANT ALL ON SCHEMA public TO chaimk;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

