class CriteriaRanking < ActiveRecord::Base
	belongs_to :criterium
	belongs_to :user
	
	# Make sure the foreign key associations and are valid:
	validates :user, :criterium, presence: true
	
	# Make sure the ranking field is non-empty and contains a number greater than 0:
	validates :ranking, numericality: { greater_than: 0 }
	
	# Make sure that there is only *one* combination of criterium_id & user_id:
	validates :criterium_id, uniqueness: { scope: :user_id, message: 
		"There is already an existing Criterium Ranking for this User." }

	default_scope { order(criterium_id: :asc) }

	# Return the numerical ranking for a given user and criterium
	def self.get_ranking_where(args)
		if args[:user_id] and args[:criterium_id] then	# expects a hash with :user_id and :criterium_id key-values
			cr = CriteriaRanking.find_by(args)
			if cr then return cr.ranking end 				# return the numerical ranking
		end
	end

end
