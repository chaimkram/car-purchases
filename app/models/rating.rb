class Rating < ActiveRecord::Base
	belongs_to :user
	belongs_to :criterium
	belongs_to :car
	
	# Make sure the foreign key associations are valid:
	validates :user, :criterium, :car, presence: true
	
	# Make sure that there is only *one* combination of criterium_id, user_id, & car_id:
	validates :criterium_id, uniqueness: { scope: [:user_id, :car_id], message: 
	 "There is already an existing Rating for this Criterium by this User on this Car." }

	# Make sure the rating field is non-empty and contains a number greater than 0:
	validates :rating, numericality: { greater_than: 0 }
	

	def weighted_rating	# Returns a number between 1 and 5 or nil if no CriteriaRanking was found

		# Locate the CriteriaRanking for the current rating's user_id and criterium_id:
		cr = CriteriaRanking.find_by(user_id: self.user_id, criterium_id: self.criterium_id)
		
		if cr.present? then  # there is a ranking for this user & criterium
			
			# Get the average ranking for this user across all criteria (scale of 0-4):
			cr_avg = CriteriaRanking.average(:ranking).to_f - 1
		
			# Compute and return the weighted rating:
			return ( (self.rating - 1) * (cr.ranking - 1) / cr_avg + 1 )
		end
	end
end
