class Car < ActiveRecord::Base
	has_many :ratings, dependent: :destroy
	
	validates :name, uniqueness: { case_sensitive: false }

end
