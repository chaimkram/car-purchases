class Criterium < ActiveRecord::Base
  has_many :criteria_rankings, dependent: :destroy
  has_many :ratings, dependent: :destroy
  has_many :users, :through => :criteria_rankings
  accepts_nested_attributes_for :criteria_rankings
end
