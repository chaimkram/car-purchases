class User < ActiveRecord::Base
	has_many :ratings, dependent: :destroy
	has_many :criteria_rankings, dependent: :destroy
	has_many :criteria, :through => :criteria_rankings
	accepts_nested_attributes_for :criteria_rankings
	
	validates :name, uniqueness: { case_sensitive: false }
	
end
