json.array!(@cars) do |car|
  json.extract! car, :id, :name, :year, :price, :kilometrage, :current_owner, :phone, :address
  json.url car_url(car, format: :json)
end
