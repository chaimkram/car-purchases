json.array!(@criteria) do |criterium|
  json.extract! criterium, :id, :name, :comments
  json.url criterium_url(criterium, format: :json)
end
