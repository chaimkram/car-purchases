class CriteriaRankingsController < ApplicationController
	@my_params
	before_action :set_criteria_ranking, only: [:show, :edit, :update, :destroy]

	# GET /criteria_rankings
	# GET /criteria_rankings.json
	# def index
	# 	@criteria_rankings = CriteriaRanking.all
	# end

	# GET /criteria_rankings/1
	# GET /criteria_rankings/1.json
	# def show
# 	end

	# GET /criteria_rankings/new
	def new
		@criteria_ranking = CriteriaRanking.new(user_id: params[:user_id], criterium_id: params[:criterium_id])
	end

	# GET /criteria_rankings/1/edit
	def edit
	end

	# POST /criteria_rankings
	# POST /criteria_rankings.json
	def create
		@criteria_ranking = CriteriaRanking.new(criteria_ranking_params)

		respond_to do |format|
			if @criteria_ranking.save
				format.html { redirect_to criteria_path, notice: 'Criteria ranking was successfully created.' }
				format.json { render :show, status: :created, location: @criteria_ranking }
			else
				format.html { render :new }
				format.json { render json: @criteria_ranking.errors, status: :unprocessable_entity }
			end
		end
	end

	# PATCH/PUT /criteria_rankings/1
	# PATCH/PUT /criteria_rankings/1.json
	def update
		respond_to do |format|
			if @criteria_ranking.update(criteria_ranking_params)
				format.html { redirect_to criteria_path, notice: 'Criteria ranking was successfully updated.' }
				format.json { render :show, status: :ok, location: @criteria_ranking }
			else
				format.html { render :edit }
				format.json { render json: @criteria_ranking.errors, status: :unprocessable_entity }
			end
		end
	end

	# DELETE /criteria_rankings/1
	# DELETE /criteria_rankings/1.json
	def destroy
		@criteria_ranking.destroy
		respond_to do |format|
			format.html { redirect_to criteria_rankings_url, notice: 'Criteria ranking was successfully destroyed.' }
			format.json { head :no_content }
		end
	end

	private
		# Use callbacks to share common setup or constraints between actions.
		def set_criteria_ranking
			@criteria_ranking = CriteriaRanking.find(params[:id])
		end

		# Never trust parameters from the scary internet, only allow the white list through.
		def criteria_ranking_params
			params.require(:criteria_ranking).permit(:criterium_id, :user_id, :ranking, :notes)
		end
end
