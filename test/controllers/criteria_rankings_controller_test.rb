require 'test_helper'

class CriteriaRankingsControllerTest < ActionController::TestCase
  setup do
    @criteria_ranking = criteria_rankings(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:criteria_rankings)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create criteria_ranking" do
    assert_difference('CriteriaRanking.count') do
      post :create, criteria_ranking: { criteria_id: @criteria_ranking.criteria_id, ranking: @criteria_ranking.ranking, user_id: @criteria_ranking.user_id }
    end

    assert_redirected_to criteria_ranking_path(assigns(:criteria_ranking))
  end

  test "should show criteria_ranking" do
    get :show, id: @criteria_ranking
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @criteria_ranking
    assert_response :success
  end

  test "should update criteria_ranking" do
    patch :update, id: @criteria_ranking, criteria_ranking: { criteria_id: @criteria_ranking.criteria_id, ranking: @criteria_ranking.ranking, user_id: @criteria_ranking.user_id }
    assert_redirected_to criteria_ranking_path(assigns(:criteria_ranking))
  end

  test "should destroy criteria_ranking" do
    assert_difference('CriteriaRanking.count', -1) do
      delete :destroy, id: @criteria_ranking
    end

    assert_redirected_to criteria_rankings_path
  end
end
