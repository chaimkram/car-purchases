Car Purchase App Description
================

This app assists with the prospective purchase of a car. You can add users, criteria
on which the car will be judged, and ranking of those criteria per user. Then you can add
entries for each car listing, and each user can provide individual ratings for each 
criterion for that car. Finally, a weighted rating is computed for each user on that car.

Ruby on Rails
-------------

This application requires:

- Ruby 2.1.1
- Rails 4.2.4

Learn more about [Installing Rails](http://railsapps.github.io/installing-rails.html).

[![Deploy to Heroku](https://www.herokucdn.com/deploy/button.png)](https://heroku.com/deploy)

Getting Started
---------------
1. Add some Users
2. Add some Criteria on which your purchase will be based
3. Each user must rank the Criteria on a scale of 1–5
4. Enter a Car which you are considering purchasing
5. Each User must rate the Car's Criteria on a scale of 1–5
1. As ratings are entered, the total weighted rating for that car is computed automatically for each user

Documentation and Support
-------------------------
I hope you enjoy my little app. Documentation, what there is of it, is in the /docs directory. Send feedback to [chaimk@umich.edu](mailto:chaimk@umich.edu).

Issues
-------------

Ancillary Info
==============

This application was generated with the [rails_apps_composer](https://github.com/RailsApps/rails_apps_composer) gem
provided by the [RailsApps Project](http://railsapps.github.io/).

Rails Composer is open source and supported by subscribers. Please join RailsApps to support development of Rails Composer.

Problems? Issues?
-----------

Need help? Ask on Stack Overflow with the tag 'railsapps.'

Your application contains diagnostics in the README file. Please provide a copy of the README file when reporting any issues.

If the application doesn't work as expected, please [report an issue](https://github.com/RailsApps/rails_apps_composer/issues)
and include the diagnostics.

License
-------
© Copyright Chaim Kram 2015