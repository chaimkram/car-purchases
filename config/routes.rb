Rails.application.routes.draw do
  resources :cars
  resources :ratings
  resources :criteria_rankings
  resources :users
  resources :criteria
  root to: 'pages#about'
end
