class ChangeCriteriumCommentsToText < ActiveRecord::Migration
  def change
    change_column :criteria, :comments, :text
  end
end
