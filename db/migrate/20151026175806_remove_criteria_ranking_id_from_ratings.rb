class RemoveCriteriaRankingIdFromRatings < ActiveRecord::Migration
  def change
    remove_column :ratings, :criteria_ranking_id, :integer
  end
end
