class AddNotesFieldToCriteriaRankings < ActiveRecord::Migration
  def change
    add_column :criteria_rankings, :notes, :text
  end
end
