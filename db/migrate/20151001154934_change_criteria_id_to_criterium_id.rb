class ChangeCriteriaIdToCriteriumId < ActiveRecord::Migration
  def change
    rename_column :criteria_rankings, :criteria_id, :criterium_id
  end
end
