class AddUniqueUserCriteriumIndexToCriteriaRankings < ActiveRecord::Migration
  def change
    add_index :criteria_rankings, [:criterium_id, :user_id], unique: true
  end
end
