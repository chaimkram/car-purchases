class CreateCriteriaRankings < ActiveRecord::Migration
  def change
    create_table :criteria_rankings do |t|
      t.integer :criteria_id
      t.integer :user_id
      t.integer :ranking

      t.timestamps null: false
    end
  end
end
