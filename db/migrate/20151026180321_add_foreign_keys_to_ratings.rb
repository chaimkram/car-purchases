class AddForeignKeysToRatings < ActiveRecord::Migration
  def change
    add_foreign_key :ratings, :users
    add_foreign_key :ratings, :criteria
    add_foreign_key :ratings, :cars
  end
end
