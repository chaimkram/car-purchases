class CreateCars < ActiveRecord::Migration
  def change
    create_table :cars do |t|
      t.string :name
      t.string :year
      t.integer :price
      t.integer :kilometrage
      t.string :current_owner
      t.integer :phone
      t.string :address

      t.timestamps null: false
    end
  end
end
