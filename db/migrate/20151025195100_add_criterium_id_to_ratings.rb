class AddCriteriumIdToRatings < ActiveRecord::Migration
  def change
    add_column :ratings, :criterium_id, :integer
  end
end
